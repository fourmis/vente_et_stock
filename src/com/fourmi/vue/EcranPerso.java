package com.fourmi.vue;

import com.fourmi.controleur.EnumTypeEcran;
import com.fourmi.controleur.Session;
import com.fourmi.metier.Client;
import com.fourmi.metier.Produit;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EcranPerso {

    public JFrame frame;

    Client client = null;
    Produit produit = null;

    public EcranPerso(Client client, Produit produit) {
        this.client =client;
        this.produit =produit;
        afficherEcranPerso();
    }

    private void afficherEcranPerso() {
        frame = new JFrame();
        frame.setTitle("French Chic - Accueil Perso");
        frame.setSize(650, 500);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
        JPanel accueilPanel = new JPanel();
        accueilPanel.setBackground(Color.WHITE);
        frame.setContentPane(accueilPanel);
        frame.setLayout(null);

        JLabel title = new JLabel("French Chic");
        title.setLocation(150, 50);
        title.setSize(1000, 100);
        Font f = new Font("", Font.PLAIN, 70);
        title.setFont(f);
        title.setForeground(Color.MAGENTA);

        JLabel clientInfoLabel = null;
        JLabel produitInfoLabel = null;
        JLabel quantiteInfoLabel = null;
        JLabel quantiteInfo2Label = null;
        JTextField quantiteField =null;

        clientInfoLabel = new JLabel( String.format("Bonjour %s %s",client.getNom(), client.getPrenom()));
        clientInfoLabel.setSize(300, 20);
        clientInfoLabel.setLocation(150, 200);

        produitInfoLabel = new JLabel(String.format("Le produit du jour est le \"%s\" au prix de %s€ ",produit.getLibelle(), String.valueOf(produit.getPrix())));
        produitInfoLabel.setSize(400, 20);
        produitInfoLabel.setLocation(150, 250);

        quantiteInfoLabel = new JLabel(String.format("Quantité en stock : %s  ", String.valueOf(produit.getQuantiteEnStock())));
        quantiteInfoLabel.setSize(300, 20);
        quantiteInfoLabel.setLocation(150, 270);

        quantiteInfo2Label = new JLabel("Quantité ");
        quantiteInfo2Label.setSize(100, 20);
        quantiteInfo2Label.setLocation(180, 320);

        quantiteField = new JTextField();
        quantiteField.setSize(70, 20);
        quantiteField.setLocation(250, 320);

        JButton ajouter = new JButton("Ajouter le produit du jour au panier");
        ajouter.setLocation(180, 350);
        ajouter.setSize(300, 30);

        frame.add(title);
        frame.add(clientInfoLabel);
        frame.add(produitInfoLabel);
        frame.add(quantiteInfoLabel);
        frame.add(quantiteInfo2Label);
        frame.add(quantiteField);
        frame.add(ajouter);
        frame.setVisible(true);

    }



}
