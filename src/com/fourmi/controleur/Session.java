package com.fourmi.controleur;

import com.fourmi.metier.Client;
import com.fourmi.metier.Commande;
import com.fourmi.metier.Produit;

public class Session {

    public EnumTypeEcran traiterConnexion(){
        return EnumTypeEcran.ECRAN_ACCUEIL;
    }

    public ReponseTraiterIdentification traiterIdentification(String pseudo, String motDePasse){
        Client client = Client.rechercherClient(pseudo, motDePasse);
        Produit produit = Produit.rechercherProduitDuJour();
        return new ReponseTraiterIdentification(EnumTypeEcran.ECRAN_PERSO, client, produit);
    }

    public TraiterAjoutPanierReponse traiterAjoutPanier(Produit produit, int intg){
        TraiterAjoutPanierReponse traiterAjoutPanierReponse = new TraiterAjoutPanierReponse();
        Commande commande = new Commande();
        commande.ajouterProduit(produit, intg);
        traiterAjoutPanierReponse.typeEcran = EnumTypeEcran.ECRAN_PANIER;
        traiterAjoutPanierReponse.laCommande = commande;
        return traiterAjoutPanierReponse;
    }

}
