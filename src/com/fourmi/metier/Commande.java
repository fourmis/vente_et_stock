package com.fourmi.metier;

import java.util.ArrayList;

public class Commande {

    public static ArrayList<Commande> lesCommande = new ArrayList();

    private String numero ;
    private float montant ;
    private ArrayList<LigneCommande> lesLignesCommande;

    public Commande() {
        this.numero = "CM"+lesCommande.size()+1;
        this.montant = 0 ;
        lesLignesCommande=new ArrayList<>();
    }

    public void ajouterProduit(Produit leProduit, int quantite ){
        LigneCommande  ligneCommande = new LigneCommande(quantite, leProduit.getPrix()*quantite, leProduit);
        leProduit.retirerDuStock(quantite);
        this.lesLignesCommande.add(ligneCommande);
        this.montant= this.montant+ligneCommande.getMontant();
    }


    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public float getMontant() {
        return montant;
    }

    public void setMontant(float montant) {
        this.montant = montant;
    }

    public ArrayList<LigneCommande> getLesLignesCommande() {
        return lesLignesCommande;
    }

    public void setLesLignesCommande(ArrayList<LigneCommande> lesLignesCommande) {
        this.lesLignesCommande = lesLignesCommande;
    }
}
