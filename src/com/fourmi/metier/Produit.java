package com.fourmi.metier;

import java.util.ArrayList;

public class Produit {

    public static ArrayList<Produit> lesProduits = new ArrayList(){{
        add(new Produit("P001", "Pantalon Zouk", 50.0f, true, 54));
        add(new Produit("P002","Maillot Zouk", 40.0f, false, 30));
        add(new Produit("P003","Bermuda", 100.0f, false, 60));
    }};

    private String reference;
    private String libelle;
    private float prix;
    private boolean estDuJour;
    private int quantiteEnStock;

    public static Produit rechercherProduitDuJour(){
        Produit produit = null;
        for (Produit produitTemp: lesProduits) {
            if(produitTemp.isEstDuJour()){
                produit=produitTemp;
            }
        }
        return produit;
    }

    public boolean estEnStock(int quantite){
        return  this.quantiteEnStock>=quantite;
    }

    public void retirerDuStock(int quantite){
        for (Produit produitTemp: lesProduits ) {
            if(produitTemp.equals(this.reference)){
                produitTemp.setQuantiteEnStock(produitTemp.getQuantiteEnStock() - quantite);
            }
        }
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public boolean isEstDuJour() {
        return estDuJour;
    }

    public void setEstDuJour(boolean estDuJour) {
        this.estDuJour = estDuJour;
    }

    public int getQuantiteEnStock() {
        return quantiteEnStock;
    }

    public void setQuantiteEnStock(int quantiteEnStock) {
        this.quantiteEnStock = quantiteEnStock;
    }

    public Produit(String reference, String libelle, float prix, boolean estDuJour, int quantiteEnStock) {
        this.reference = reference;
        this.libelle = libelle;
        this.prix = prix;
        this.estDuJour = estDuJour;
        this.quantiteEnStock = quantiteEnStock;
    }
}
